﻿using System;
using System.Collections.Generic;
using System.Text;
using LINQ.Models;

namespace LINQ.Models
{
    class HelperModel1
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int TasksCount { get; set; }
        public int CanceledAndUnfinishedTasksCount { get; set; }
        public Task LongestTask { get; set; }

        public override string ToString()
        {
            return new string($"User: {User.ToString()}\nLast project: {LastProject.ToString()}\nCount of tasks: {TasksCount}\nUnfinished or canceled tasks: {CanceledAndUnfinishedTasksCount}\nLongest task: {LongestTask.ToString()}");
        }
    }
}
