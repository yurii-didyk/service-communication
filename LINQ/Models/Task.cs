﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace LINQ.Models
{
    class Task
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }
        [JsonProperty("finished_at")]
        public DateTime FinishedAt { get; set; }
        [JsonProperty("state")]
        public TaskStates State { get; set; }
        [JsonProperty("project_id")]
        public int ProjectId { get; set; }
        [JsonProperty("performer_id")]
        public int PerformerId { get; set; }
        //public User Performer { get; set; }
        public override string ToString()
        {
            return new string($"TaskId: {Id}\nTask name: {Name}\nTask description: {Description}\nCreatedAt: {CreatedAt}\nFinishedAt: {FinishedAt}\nState: {(TaskStates)State}\nProjectId: {ProjectId}\nPerformerId: {PerformerId}");
        }
    }
    enum TaskStates
    {
        Created,
        Started,
        Finished,
        Canceled
    }
}
