﻿using System;
using System.Collections.Generic;
using System.Text;
using LINQ.Models;
using System.Linq;
using System.Net.Http.Headers;
using System.Net.Http;
using Newtonsoft.Json;

namespace LINQ
{
    static class HelperHttpMethods
    {
        public static HttpResponseMessage GetHttpResponse(string url)
        {
            HttpResponseMessage response;
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                response = client.GetAsync(url).Result;
            }
            return response;
        }
        public static HttpResponseMessage PostHttpResponse(string url, object obj)
        {
            HttpResponseMessage response;
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                response = client.PostAsJsonAsync(url, obj).Result;
            }
            return response;
        }
    }
}
