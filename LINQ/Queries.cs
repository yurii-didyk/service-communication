﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Net.Http;
using LINQ.Models;
using ProjectStructureWebAPI;

namespace LINQ
{
    class Queries
    {
        private static readonly AppConfiguration config = new AppConfiguration();
        private static string URL = config.Server + "api";
       
        public static List<TaskCountForProject> TasksCountForProject(int userId)
        {
            var result = JsonConvert.DeserializeObject<List<TaskCountForProject>>(HelperHttpMethods.GetHttpResponse($"{URL}/queries/1/{userId}").Content.ReadAsStringAsync().Result);
            return result;
            
        }
        // 2 querry
        public static List<Task> TasksForPerformer(int userId)
        {
            var result = JsonConvert.DeserializeObject<List<Task>>(HelperHttpMethods.GetHttpResponse($"{URL}/queries/2/{userId}").Content.ReadAsStringAsync().Result);
            return result;
        }
        // 3 querry

        public static List<Tuple<int, string>> FinishedTasksInThisYear(int userId)
        { 
            var result = JsonConvert.DeserializeObject<List<Tuple<int,string>>>(HelperHttpMethods.GetHttpResponse($"{URL}/queries/3/{userId}").Content.ReadAsStringAsync().Result);
            return result;
        }
        // 4 querry

        public static List<Tuple<int, string, List<User>>> TeamsOlder12()
        {
            var result = JsonConvert.DeserializeObject<List<Tuple<int, string, List<User>>>>(HelperHttpMethods.GetHttpResponse($"{URL}/queries/4").Content.ReadAsStringAsync().Result);
            return result;
        }

        // 5 querry 
        public static List<Tuple<User, List<Task>>> GetSortedUsers()
        {
            var result = JsonConvert.DeserializeObject<List<Tuple<User, List<Task>>>>(HelperHttpMethods.GetHttpResponse($"{URL}/queries/5").Content.ReadAsStringAsync().Result);
            return result;
        }
        // 6 querry
        public static HelperModel1 GetModel1(int userId)
        {
            var result = JsonConvert.DeserializeObject<HelperModel1>(HelperHttpMethods.GetHttpResponse($"{URL}/queries/6/{userId}").Content.ReadAsStringAsync().Result);
            return result;
        }
        // 7 querry 
        public static HelperModel2 GetModel2(int projectId)
        {
            var result = JsonConvert.DeserializeObject<HelperModel2>(HelperHttpMethods.GetHttpResponse($"{URL}/queries/7/{projectId}").Content.ReadAsStringAsync().Result);
            return result;
        }
    }
}
