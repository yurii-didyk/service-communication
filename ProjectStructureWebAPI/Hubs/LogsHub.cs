﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace ProjectStructureWebAPI.Hubs
{
    public class LogsHub: Hub
    {
        public async Task Send(string value)
        {
            await Clients.All.SendAsync("LogsStart", value);
        }
    }
}
