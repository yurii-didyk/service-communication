﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructureWebAPI.DTOs
{
    public class TaskCountForProjectDTO
    {
        public ProjectDTO Project { get; set; }
        public int TasksCount { get; set; }
    }
}
