﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructureWebAPI.Repository
{
    public interface IRepository<T> where T: class
    {
        IEnumerable<T> GetItems();
        T GetSingleItem(int id);
        void Create(T entity);
        void Update(int id, T entity);
        void Delete(int id);
    }
}
