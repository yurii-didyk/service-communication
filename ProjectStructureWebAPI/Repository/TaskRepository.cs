﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructureWebAPI.Models;

namespace ProjectStructureWebAPI.Repository
{
    public class TaskRepository: IRepository<Models.Task>
    {
        private List<Models.Task> tasks = new List<Models.Task>();
        private static int startId = 1;

        public IEnumerable<Models.Task> GetItems()
        {
            return tasks;
        }
        public Models.Task GetSingleItem(int id)
        {
            return tasks.FirstOrDefault(i => i.Id == id);
        }
        public void Create(Models.Task entity)
        {
            entity.Id = startId;
            startId++;
            tasks.Add(entity);
        }
        public void Update(int id, Models.Task entity)
        {
            int index = tasks.IndexOf(tasks.FirstOrDefault(e => e.Id == id));
            tasks[index] = entity;
            tasks[index].Id = id;
            
        }
        public void Delete(int id)
        {
            tasks.Remove(tasks.FirstOrDefault(i => i.Id == id));
        }
    }
}
