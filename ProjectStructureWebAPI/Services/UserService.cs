﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructureWebAPI.Repository;
using ProjectStructureWebAPI.DTOs;
using ProjectStructureWebAPI.Models;
using AutoMapper;

namespace ProjectStructureWebAPI.Services
{
    public class UserService
    {
        private readonly IRepository<User> _repository;
        private readonly IMapper _mapper;
        private readonly QueueService _queueService;


        public UserService(IRepository<User> repository,
            IMapper mapper, QueueService queueService)
        {
            _repository = repository;
            _mapper = mapper;
            _queueService = queueService;
        }

        public void Add(UserDTO teamDto)
        {
            _queueService.Post("User creation was triggered");
            var user = _mapper.Map<User>(teamDto);
            _repository.Create(user);
        }
        public void Delete(int id)
        {
            _queueService.Post("User deletion was triggered");
            _repository.Delete(id);
        }
        public void Update(int id, UserDTO teamDto)
        {
            _queueService.Post("User updating was triggered");
            var user = _mapper.Map<User>(teamDto);
            _repository.Update(id, user);
        }
        public UserDTO GetSingle(int id)
        {
            _queueService.Post("Loading single user was triggered");
            var user = _repository.GetSingleItem(id);
            return _mapper.Map<UserDTO>(user);
        }
        public IEnumerable<UserDTO> Get()
        {
            _queueService.Post("Loading all users was triggered");
            var users = _repository.GetItems();
            return _mapper.Map<IEnumerable<UserDTO>>(users);
        }
    }
}