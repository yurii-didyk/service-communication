﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructureWebAPI.Repository;
using ProjectStructureWebAPI.DTOs;
using ProjectStructureWebAPI.Models;
using AutoMapper;

namespace ProjectStructureWebAPI.Services
{
    public class TeamService
    {
        private readonly IRepository<Team> _repository;
        private readonly IMapper _mapper;
        private readonly QueueService _queueService;

        public TeamService(IRepository<Team> repository,
            IMapper mapper, QueueService queueService)
        {
            _repository = repository;
            _mapper = mapper;
            _queueService = queueService;
        }

        public void Add(TeamDTO teamDto)
        {
            _queueService.Post("Team creating was triggered");
            var team = _mapper.Map<Team>(teamDto);
            _repository.Create(team);
        }
        public void Delete(int id)
        {
            _queueService.Post("Team deletion was triggered");
            _repository.Delete(id);
        }
        public void Update(int id, TeamDTO teamDto)
        {
            _queueService.Post("Team updating was triggered");
            var team = _mapper.Map<Team>(teamDto);
            _repository.Update(id, team);
        }
        public TeamDTO GetSingle(int id)
        {
            _queueService.Post("Loading single team was triggered");
            var team = _repository.GetSingleItem(id);
            return _mapper.Map<TeamDTO>(team);
        }
        public IEnumerable<TeamDTO> Get()
        {
            _queueService.Post("Loading all teams was triggered");
            var teams = _repository.GetItems();
            return _mapper.Map<IEnumerable<TeamDTO>>(teams);
        }
    }
}
