﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Microsoft.Extensions.Configuration;
using ProjectStructureWebAPI.QueueServices;
using ProjectStructureWebAPI.Interfaces;
using ProjectStructureWebAPI.MessageSettingsModels;
using ProjectStructureWebAPI.Hubs;
using Microsoft.AspNetCore.SignalR;

namespace ProjectStructureWebAPI.Services
{
    public class QueueService
    {

        private readonly IMessageConsumerScope _messageConsumerScope;
        private readonly IMessageProducerScope _messageProducerScope;
        private readonly IHubContext<MessagesHub> _hub;
        
        public QueueService(IMessageProducerScopeFactory messageProducerScopeFactory,
            IMessageConsumerScopeFactory messageConsumerScopeFactory,
            IHubContext<MessagesHub> hub)
        {
            _hub = hub;
            _messageConsumerScope = messageConsumerScopeFactory.Connect(new MessageScopeSettings
            {
                ExchangeName = "MainExchange",
                ExchangeType = ExchangeType.Direct,
                QueueName = "ResultQueue",
                RoutingKey = "result"
            });
            _messageProducerScope = messageProducerScopeFactory.Open(new MessageScopeSettings
            {
                ExchangeName = "MainExchange",
                ExchangeType = ExchangeType.Direct,
                QueueName = "MethodQueue",
                RoutingKey = "method"
            });
            _messageConsumerScope.MessageConsumer.Received += Get;
        }

        public bool Post(string msg)
        {
            try
            {
                _messageProducerScope.MessageProducer.Send(msg);
                return true;
            }
            catch(Exception)
            {
                return false;
            }
        }

        private void Get(object sender, BasicDeliverEventArgs args)
        {
            string msg = Encoding.UTF8.GetString(args.Body);
            _hub.Clients.All.SendAsync("SendResponse", msg);

            _messageConsumerScope.MessageConsumer.SetAcknowledge(args.DeliveryTag, true);
        }
                
        
        
    }
}
