﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using ProjectStructureWebAPI.Hubs;
using ProjectStructureWebAPI.DTOs;

namespace ProjectStructureWebAPI.Services
{
    public class LogsService
    {
        private readonly IHubContext<LogsHub> _hub;
        List<MessageModelDTO> logs = new List<MessageModelDTO>();

        public LogsService(IHubContext<LogsHub> hub)
        {
            _hub = hub;
        }

        public void SendRequestToRecieveLogs()
        {
            _hub.Clients.All.SendAsync("Logs");
            System.Threading.Thread.Sleep(500);
            _hub.Clients.All.SendAsync("LogsReceived", logs);
        }

        public void CreateLogs(IEnumerable<MessageModelDTO> args)
        {
            logs = (List<MessageModelDTO>)args;
        }
    }
}
