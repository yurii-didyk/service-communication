﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructureWebAPI.Repository;
using ProjectStructureWebAPI.DTOs;
using ProjectStructureWebAPI.Models;
using AutoMapper;

namespace ProjectStructureWebAPI.Services
{
    public class ProjectService
    {
        private readonly IRepository<Project> _repository;
        private readonly IMapper _mapper;
        private readonly QueueService _queueService;

        public ProjectService(IRepository<Project> repository,
            IMapper mapper, QueueService queueService)
        {
            _repository = repository;
            _mapper = mapper;
            _queueService = queueService;
        }

        public void Add(ProjectDTO projectDto)
        {
            _queueService.Post("Project creation was triggered");
            var project = _mapper.Map<Project>(projectDto);
            _repository.Create(project);
        }
        public void Delete(int id)
        {
            _queueService.Post("Project deletion was triggered");
            _repository.Delete(id);
        }
        public void Update(int id, ProjectDTO projectDto)
        {
            _queueService.Post("Project updating was triggered");
            var project = _mapper.Map<Project>(projectDto);
            _repository.Update(id, project);
        }
        public ProjectDTO GetSingle(int id)
        {
            _queueService.Post("Loading single project was triggered");
            var project = _repository.GetSingleItem(id);
            return _mapper.Map<ProjectDTO>(project);
        }
        public IEnumerable<ProjectDTO> Get()
        {
            _queueService.Post("Loading all projects was triggered");
            var projects = _repository.GetItems();
            return _mapper.Map<IEnumerable<ProjectDTO>>(projects);
        }
    }
}
