﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructureWebAPI.Repository;
using ProjectStructureWebAPI.DTOs;
using AutoMapper;

namespace ProjectStructureWebAPI.Services
{
    public class TaskService
    {
        private readonly IRepository<Models.Task> _repository;
        private readonly IMapper _mapper;
        private readonly QueueService _queueService;

        public TaskService(IRepository<Models.Task> repository,
            IMapper mapper, QueueService queueService)
        {
            _repository = repository;
            _mapper = mapper;
            _queueService = queueService;
        }

        public void Add(TaskDTO taskDto)
        {
            _queueService.Post("Task creation was triggered");
            var task = _mapper.Map<Models.Task>(taskDto);
            _repository.Create(task);
        }
        public void Delete(int id)
        {
            _queueService.Post("Task deletion was triggered");
            _repository.Delete(id);
        }
        public void Update(int id, TaskDTO taskDto)
        {
            _queueService.Post("Task updating was triggered");
            var task = _mapper.Map<Models.Task>(taskDto);
            _repository.Update(id, task);
        }
        public TaskDTO GetSingle(int id)
        {
            _queueService.Post("Loading single task was triggered");
            var task = _repository.GetSingleItem(id);
            return _mapper.Map<TaskDTO>(task);
        }
        public IEnumerable<TaskDTO> Get()
        {
            _queueService.Post("Loading all tasks was triggered");
            var tasks = _repository.GetItems();
            return _mapper.Map<IEnumerable<TaskDTO>>(tasks);
        }

    }
}
