﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectStructureWebAPI.Services;
using ProjectStructureWebAPI.DTOs;

namespace ProjectStructureWebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly TeamService _teamService;

        public TeamsController(TeamService teamService)
        {
            _teamService = teamService;

        }
        [HttpGet]
        public IEnumerable<TeamDTO> GetTeams()
        {
            return _teamService.Get();
        }
        [HttpGet("{id}")]
        public TeamDTO Get([FromBody] int id)
        {
            return _teamService.GetSingle(id);
        }
        [HttpPost]
        public void Add([FromBody] TeamDTO teamDto)
        {
            _teamService.Add(teamDto);
        }
        [HttpDelete("{id}")]
        public void Delete([FromBody] int id)
        {
            _teamService.Delete(id);
        }
        [HttpPut("{id}")]
        public void Update(int id, [FromBody] TeamDTO teamDto)
        {
            _teamService.Update(id, teamDto);
        }

    }
}