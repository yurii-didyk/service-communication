﻿using System;
using System.Collections.Generic;
using System.Text;
using Worker.Models;

namespace Worker
{
    interface ILogger
    {
        void WriteLog(MessageModel model);
        IEnumerable<MessageModel> ReadLogAsCollection();
    }
}
