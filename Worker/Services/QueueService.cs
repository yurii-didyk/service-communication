﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Configuration;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Worker.Models;
using ProjectStructureWebAPI.MessageSettingsModels;
using ProjectStructureWebAPI.QueueServices;
using ProjectStructureWebAPI.Interfaces;

namespace Worker.Services
{
    class QueueService: IDisposable
    {
        private readonly IMessageConsumerScope _messageConsumerScope;
        private readonly IMessageProducerScope _messageProducerScope;
        private readonly ILogger _logger;

        public QueueService(IMessageProducerScopeFactory messageProducerScopeFactory,
            IMessageConsumerScopeFactory messageConsumerScopeFactory)    
        {
            _messageConsumerScope = messageConsumerScopeFactory.Connect(new MessageScopeSettings
            {
                ExchangeName = "MainExchange",
                ExchangeType = ExchangeType.Direct,
                QueueName = "MethodQueue",
                RoutingKey = "method"
            });
                
            _messageProducerScope = messageProducerScopeFactory.Open(new MessageScopeSettings
            {
                ExchangeName = "MainExchange",
                ExchangeType = ExchangeType.Direct,
                QueueName = "ResultQueue",
                RoutingKey = "result"
            });
            _logger = new Logger();
            _messageConsumerScope.MessageConsumer.Received += OnMessageReceived;
        }

        public void Run()
        {
            Console.WriteLine("Service run");
        }

        private void OnMessageReceived(object sender, BasicDeliverEventArgs args)
        {
            var processed = false;
            try
            {
                var body = args.Body;
                var msg = Encoding.UTF8.GetString(body);
                Console.WriteLine(msg);
                _logger.WriteLog(new MessageModel
                {
                    Message = msg,
                    CreatedAt = DateTime.Now
                });
                processed = true;
                SendSuccesfullMessage(msg);
            }
            catch (Exception e)
            {
                SendExceptionMessage(e.Message);
            }
            finally
            {
                _messageConsumerScope.MessageConsumer.SetAcknowledge(args.DeliveryTag, processed);
            }
        }

        public void Dispose()
        {
            _messageConsumerScope.Dispose();
            _messageProducerScope.Dispose();
        }

        private void SendSuccesfullMessage(string msg)
        {
            _messageProducerScope.MessageProducer.Send($"Succesfully received: {msg}");
        }
        private void SendExceptionMessage(string msg)
        {
            _messageProducerScope.MessageProducer.Send($"Failed during handling message: {msg}");
        }
    }
}
