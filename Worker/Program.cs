﻿using System;
using Worker.Services;
using Worker.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ProjectStructureWebAPI.Interfaces;
using ProjectStructureWebAPI.QueueServices;
using RabbitMQ.Client;
using ProjectStructureWebAPI;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Collections.Generic;
using Newtonsoft.Json;
using Microsoft.AspNetCore.SignalR.Client;

namespace Worker
{
    class Program
    {
        static void Main(string[] args)
        {
            SignalRRun().Wait();
            var IoC = new IoC();
        }
        static async System.Threading.Tasks.Task SignalRRun()
        {
            var config = new AppConfiguration();
            var connection = new HubConnectionBuilder()
               .WithUrl($"{config.Server}logs")
               .Build();
            connection.On("Logs", () =>
            {
                var collection = new Logger().ReadLogAsCollection();
                PostHttpResponse(config.Server + "api/logs", collection);
            });
            await connection.StartAsync();
        }

        public static HttpResponseMessage PostHttpResponse(string url, object obj)
        {
            HttpResponseMessage response;
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                response = client.PostAsJsonAsync(url, obj).Result;
            }
            return response;
        }

    }
}
